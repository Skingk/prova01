public class ControllerDisciplina{
	private ArrayList<Disciplina> listaDisciplinas;
	private ArrayList<Aluno> listaAlunos;
	private Disciplina umaDisciplina;
	
	public ControllerDisciplina(){
		this.listaDisciplinas = new ArrayList<Disciplina>();
		this.umaDisciplina = new Disciplina();
	}

	public String adicionar(Disciplina umaDisciplina){
		this.listaDisciplinas.add(umaDisciplina);
		return "Disciplina adicionada com sucesso";
	}
	
	public String adicionarAluno(Aluno umAluno){
		this.listaAlunos.add(umAluno);
		this.umaDisciplina.setListaAluno = this.listaAlunos();
		return "Aluno adicionado a disciplina com sucesso";
	}

	public String excluir(Disciplina umaDisciplina){
		this.listaDisciplinas.remove(umaDisciplina);
		return "Disciplina excluida com sucesso";
	}

	public Disciplina localizar(String nome){
		for(Disciplina umaDisciplina : listaDisciplinas){
			if(umaDisciplina.getNome().equalsIgnoreCase(nome)){
				return umaDisciplina;			
			}
			return null;
		}
	}
}
